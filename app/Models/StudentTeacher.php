<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentTeacher extends Model
{
    use HasFactory;
    public $timestamps = false;

    public function getTeacherData()
    {
        return $this->belongsTo(User::class,'teacher_id','id');
    }
    public function getUserData()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
