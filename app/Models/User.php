<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens;

    protected $guard = 'user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'address',
        'email_verified_at',
        'profile',
        'parent_detail',
        'teacher_id',
        'active',
        'fcm_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function findForPassport($username)
    {
        return $this->where('email', $username)->first();
    }

    public function getStudent()
    {
        return $this->hasMany(StudentTeacher::class,'teacher_id','id');
    }
    public function getTeacher()
    {
        return $this->hasOne(StudentTeacher::class,'user_id','id');
    }

    public function getTeacherDetails()
    {
        return $this->hasOne(TeacherDetail::class,'user_id','id');
    }

    public function getStudentDetails()
    {
        return $this->hasOne(StudentDetail::class,'user_id','id');
    }

    public function getUserDetails()
    {
        return $this->hasOne(UserDetail::class,'user_id','id');
    }
}
