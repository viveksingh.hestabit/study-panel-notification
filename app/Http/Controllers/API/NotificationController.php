<?php

namespace App\Http\Controllers\API;

use Exception;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\VerifyUser;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Notifications\appNotification;
use App\Notifications\accountApprovalNotification;
use App\Notifications\newStudentAssignedNotification;

class NotificationController extends Controller
{
    public function sendUserApprovedNotification(VerifyUser $request)
    {
        $user = User::find($request->user_id);
        try {
            $user->notify(new accountApprovalNotification);
            $title = 'Account Approved!';
            $message = 'Your Account Approved By the Admin.';
            // FcmService::send([$user->fcm_token],['title' => $title,'body' => $message]);
            $user->notify(new appNotification($title, $message, [$user->fcm_token]));
        } catch (Exception $e) {
            Log::error($e);
        }
        return res_success('Success!');
    }

    public function sendTeacherAssignedNotification(VerifyUser $request)
    {
        $teacher = User::find($request->teacher_id);
        try {
            $teacher->notify(new newStudentAssignedNotification);
            $title = 'New Student Assigned';
            $message = 'A new Student Assigned To You';
            // FcmService::send([$teacher->fcm_token],['title' => $title,'body' => $message]);
            $teacher->notify(new appNotification($title, $message, [$teacher->fcm_token]));
        } catch (Exception $e) {
            dd($e);
            Log::error($e);
        }
        return res_success('Success!');
    }
}
