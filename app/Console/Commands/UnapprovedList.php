<?php

namespace App\Console\Commands;

use App\Jobs\SendUserList;
use Illuminate\Console\Command;

class UnapprovedList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'unapproved:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Unapproved User List';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $details['email'] = 'admin@gmail.com';
        dispatch(new SendUserList($details));
    }
}
